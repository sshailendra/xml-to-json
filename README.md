# xml-to-json is a utility jar to convert xml to json

This project uses Jaxb for readign xml and jackson for writing json.

# Maven dependency
To use this library on Maven-based projects, use following dependency

 ```
<dependency>
     <groupId>com.dataformat</groupId>
     <artifactId>xml-to-json</artifactId>
     <version>1.0-SNAPSHOT</version> 
 </dependency>
```



# Usage

 `String jsonPrettyString = XMLToJSONConverter.convert(xmlFileName)`

    
This library uses following rules for mapping between xml element to json field.

1.	‘id’ (a number) becomes ‘patientid’ (a number)
2.	‘dateOfBirth’ (a date) becomes ‘age’ (a number)
3.	‘gender’ (a letter) becomes ‘sex’ (a word)
4.	‘state’ (a full state name) becomes ‘state’ (a state abbreviation)
