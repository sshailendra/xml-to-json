package com.dataformat.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.rules.ExpectedException;

/**
 * Unit test for {@link XMLToJSONConverter} App.
 */
public class XMLToJSONConverterTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * This test expects the json file converted from xml to be equal to the actual json file.
     */
    @Test
    public void test_compareJson_equal() throws JAXBException, IOException, URISyntaxException
    {
        String patientJson = XMLToJSONConverter.convert("patient.xml");
        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource("patient.json");
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            File jsonFile = new File(resource.toURI());

            BufferedReader br = new BufferedReader(new FileReader(jsonFile));

            StringBuilder sb = new StringBuilder();
            String st;
            while ((st = br.readLine()) != null)
                sb.append(st);

            ObjectMapper mapper = new ObjectMapper();

            assertEquals(mapper.readTree(sb.toString()), mapper.readTree(patientJson));
        }
    }

    /**
     * This test expects {@link IllegalArgumentException} when invalid xml file is provided.
     */
    @Test
    public void test_compareJson_throwException() throws JAXBException, IOException, URISyntaxException
    {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("xmlFileName is invalid");
        XMLToJSONConverter.convert(null);
    }
}
