package com.dataformat.object;

import com.dataformat.adapter.AgeAdapter;
import com.dataformat.adapter.GenderAdapter;
import com.dataformat.adapter.StateAdapter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Objects;

/**
 *  A data object to map patient xml element.
 *  @author sshailendra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType (propOrder={"patientid","sex","state", "name","age"})
@JsonPropertyOrder({ "patientid", "sex", "state", "name", "age"})
public class Patient
{
    @XmlElement(name = "id")
    private  long patientid;

    @XmlElement(name = "gender")
    @XmlJavaTypeAdapter(GenderAdapter.class)
    private String sex;

    @XmlElement(name = "state")
    @XmlJavaTypeAdapter(StateAdapter.class)
    private String state;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "dateOfBirth")
    @XmlJavaTypeAdapter(AgeAdapter.class)
    private Integer age;

    public long getPatientid() {
        return patientid;
    }

    public String getSex() {
        return sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setPatientId(long patientid) {
        this.patientid = patientid;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patientid=" + patientid +
                ", sex='" + sex + '\'' +
                ", state='" + state + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return patientid == patient.patientid &&
                Objects.equals(getSex(), patient.getSex()) &&
                Objects.equals(getState(), patient.getState()) &&
                Objects.equals(getName(), patient.getName()) &&
                Objects.equals(getAge(), patient.getAge());
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientid, getSex(), getState(), getName(), getAge());
    }
}
