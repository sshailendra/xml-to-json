package com.dataformat.reader;

import com.dataformat.object.XMLRoot;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 *  Provides method to unmarshal xml to POJO {@link XMLRoot}.
 *  @author sshailendra
 */
public class XMLReader {

    /**
     * Unmarshall xml to POJO{@link XMLRoot}.
     * @param xmlFile (Cannot be null).
     *        A valid xml file.
     */
    public static XMLRoot read(String xmlFile) throws IOException, URISyntaxException, JAXBException {

        URL resource = XMLReader.class.getClassLoader().getResource(xmlFile);
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {

            File xml = new File(resource.toURI());
            JAXBContext context = JAXBContext.newInstance(XMLRoot.class);
            return  (XMLRoot) context.createUnmarshaller()
                    .unmarshal(new FileReader(xml));
        }
    }
}
