package com.dataformat.adapter;

import javax.swing.text.DateFormatter;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *  Adapter to transform dateOfBirth element to age.
 *  @author sshailendra
 */
public class AgeAdapter extends XmlAdapter<String, Integer> {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    /**
     * Convert a bound type to a value type.
     *
     * @param date The value to be convereted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public String marshal(Integer date) {
        // To be implemented when we need json to xml conversion.
        return null;
    }

    /**
     * Convert a value type to a bound type.
     *
     * @param date The value to be converted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public Integer unmarshal(String date) {
        LocalDate localdate=  LocalDate.parse(date, formatter);

        return Period.between(LocalDate.from(localdate), LocalDate.now()).getYears();
    }
}

