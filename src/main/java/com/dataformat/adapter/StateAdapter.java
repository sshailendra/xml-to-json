package com.dataformat.adapter;

import com.dataformat.object.State;

import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *  Adapter to transform state element to state abbreviation.
 *  @author sshailendra
 */
public class StateAdapter extends XmlAdapter<String, String>
{

    /**
     * Convert a value type to a bound type.
     *
     * @param state The value to be converted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    public String unmarshal(String state) throws Exception {
        return State.valueOfName(state).getAbbreviation();
    }

    /**
     * Convert a bound type to a value type.
     *
     * @param v The value to be convereted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public String marshal(String v) throws Exception {
        // To be implemented when we need json to xml conversion.
        return null;
    }

}
