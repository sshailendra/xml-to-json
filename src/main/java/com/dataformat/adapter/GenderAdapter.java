package com.dataformat.adapter;

import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *  Adapter to transform gender element to sex.
 *  @author sshailendra
 */
public class GenderAdapter extends XmlAdapter<String, String>
{

    /**
     * Convert a value type to a bound type.
     *
     * @param v The value to be converted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public String unmarshal(String v) throws Exception {
        if(v.equals("m"))
        {
            return "male";
        }
        else if(v.equals("f"))
        {
            return "female";
        }
        return null;
    }

    /**
     * Convert a bound type to a value type.
     *
     * @param v The value to be convereted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public String marshal(String v) throws Exception {
       // To be implemented when we need json to xml conversion.
        return null;
    }
}
