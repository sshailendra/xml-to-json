package com.dataformat.app;

import com.ctc.wstx.util.StringUtil;
import com.dataformat.object.XMLRoot;
import com.dataformat.reader.XMLReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 *  The main application class to convert json from given xml.
 *  @author sshailendra
 */
public class XMLToJSONConverter
{
    /**
     * Converts xml to json.
     * @param xmlFileName (Cannot be null).
     *        A valid xml file.
     */
    public static String convert(String xmlFileName) throws IOException, JAXBException, URISyntaxException
    {
        if(StringUtils.isBlank(xmlFileName))
        {
            throw new IllegalArgumentException ("xmlFileName is invalid");
        }

        XMLRoot root = XMLReader.read(xmlFileName);
        ObjectMapper mapper = new ObjectMapper();
        String jsonString =  mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root.getPatientArray());

        return jsonString;
    }
}
